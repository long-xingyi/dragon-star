import json
import base64


def string_to_base64(string):
    # 将字符串转换为字节数组
    byte_array = string.encode('utf-8')
    # 使用base64模块的b64encode函数将字节数组转换为Base64编码
    base64_string = base64.b64encode(byte_array)
    # 将Base64编码转换为字符串
    base64_string = base64_string.decode('utf-8')
    return base64_string

    
def convertTostring(base64Info):  # 将base64转换为字符串，若是非base64格式，则返回原文
    try:  # 尝试进行base64格式转换
        tmpStr = base64.b64decode(base64Info).decode("utf-8")  # 转换为字符串，UTF-8编码
        return tmpStr  # 将转换后的字符串返回
    except Exception as e:  # 非base64的情况，直接返回原内容
        return base64Info


def encode_text(text, password):
    pwd_chars = password.encode()
    text_chars = text.encode()
    index = len(text_chars)
    bytes_result = bytearray(index)
    k = 0  # 密码计时器

    for j, text_char in enumerate(text_chars):
        temp = text_char
        if temp not in (26, 0, -1, 13):  # 特殊情况不加密
            temp = text_char ^ pwd_chars[k]
        bytes_result[j] = temp
        k = (k + 1) % len(pwd_chars)  # 循环使用密码字符

    return bytes_result.decode()


def decode_text(text, password):
    pwd_chars = password.encode()
    text_chars = text.encode()
    index = len(text_chars)
    bytes_result = bytearray(index)
    k = 0  # 密码计时器

    for j, text_char in enumerate(text_chars):
        temp = text_char ^ pwd_chars[k]  # temp：读取的字节与密码异或的结果
        # 特殊情况不解密
        if temp in (26, 0, -1):
            temp = text_char
        # 换行处理
        if temp == 10:
            bytes_result[j] = 13
        bytes_result[j] = temp
        k = (k + 1) % len(pwd_chars)  # 循环使用密码字符

    return bytes_result.decode()

signd="FksYHBABLDtRU0VMcxBEWFQ6QitQWy8TCX0AA19dQF9EXlRpU1lfVGsFCgEPfR19UEYvWlYmEAlPXygsTEIjbDBeIyxqCnV3DGdyZgYBawN2GQYBW1EoWUNRSX0AByQBO1cSCxt9TA=="

# if __name__ == "__main__":
#     # text = '{"userId":0,"timestamp":"2023-04-24 08:52:04","appkey":"6EC91F3C7DB58EF58C97742EF4268E66"}'
#     # # text='{"userId"=168723,"timestamp"="2023-12-28 16:19:49","appkey"="6EC91F3C7DB58EF58C97742EF4268E66"}'
#     password = "mimouse_sign_2019_1_16_13_23"
#     # result = encode_text(text, password)
#     # print(f"加密后：{string_to_base64(result)}")
#     # print(f"解密结果{decode_text(result,password)}")
#     # sign="FksYHBABLDtRVFZYZwUCAhV9RTZcUyxFUjJCEVBLX19HR0hvQkRXV38CCQsKawttBxRzE1IvQlgIEE9SV0UgHEpYIV0cBXRzDGd0GQQOHAgEaAYBKC9ZXUNLIGlFS30="
#     # sign = "FksYHBABLDtRU0VMcxBEWFQ6QitQWy8TCX0AA19dQF9EXlRvU1heVGwGCgUKfR19UEYvWlYmEAlPXygsTEIjbDBeIyxqCnV3DGdyZgYBawN2GQYBW1EoWUNRSX0AByQBO1cSCxtnVGdUBW0JCzlTC1hQXVlHElV9Dg=="
#     # sign="FksYHBABLDtRU0VMcxBEWFQ6QitQWy8TCX0AA19dQF9EXlRvU1heVGwBCgEPfR19UEYvWlYmEAlPXygsTEIjbDBeIyxqCnV3DGdyZgYBawN2GQYBW1EoWUNRSX0AByQBO1cSCxtnVGdUBW0JCzlTC1hQXVlHElV9Dg=="
#     # sign="FksYHBABLDtRU0VfaQsIAQt9HX1FXzJUQCtTXh1LV01HQ1drXllWQ24DEAEBZQRuCwJsEx99U0MdAggWV0lHaTYqXl8ZAXMGfR0EZ3RwaglwZgUEWVsoKUFBU2c2X1FMcxBDX3owVToTDH0TTg=="
#     # sign = "FksYHBABLDtRVFZYZwUCAhV9RTZcUyxFUjJCEVBLX19HQEhuQURVV38DMAsLaQtqCRRzE1IvQlgIEE9SV0UgHEpYIV0cBXRzDGd0GQQOHAgEaAYBKC9ZXUNLIGlFS30="
#     sign="FksYHBABLDtRU0VMcxBEWFQ6QitQWy8TCX0AA19dQF9EXlRpU1lfVGsFCgEPfR19UEYvWlYmEAlPXygsTEIjbDBeIyxqCnV3DGdyZgYBawN2GQYBW1EoWUNRSX0AByQBO1cSCxt9TA=="
#     result = convertTostring(sign)
#     text = decode_text(result, password)
#     print(text)
    
#     text = '{"userId":0,"timestamp":"2023-04-24 08:52:04","appkey":"6EC91F3C7DB58EF58C97742EF4268E66"}'
#     result = string_to_base64(text)
#     password = encode_text(result,sign)
#     print("password:",password)
    
#     # timestamp = json.loads(text)['timestamp']
#     # print(timestamp)
