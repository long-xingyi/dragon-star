import gradio as gr
import sign
import json
import base64

# 设置自定义CSS来隐藏底部栏
custom_css = """
footer.svelte-1rjryqp {
    display: none !important;
}
"""

with gr.Blocks(css=custom_css,title="咪鼠加密解密工具") as demo:    
    # title = gr.title("1234")
    # gr.title("ok")
    with gr.Row() as row:
        with gr.Column():
            mode = gr.Radio(["sign", "Messages"], value="sign")
            input_letter = gr.Textbox(label="Enter letter",lines=6)
            btn1 = gr.Button("加密")
            btn2 = gr.Button("解密")
        with gr.Column():
            used_letters_box = gr.Textbox(label="Used Letters",lines=10)

    def btn_encryption(letter,mode):
        if mode == "sign":
            result = encryptionOne(letter)
        else:
            result = encryptionMul(letter)

        return {
            used_letters_box: result
        }
        
    def btn_decryption(letter,mode):
        if mode == "sign":
            result = decryptionOne(letter)
        else:
            result = decryptionMul(letter)

        return {
            used_letters_box: result
        }
    password = "mimouse_sign_2019_1_16_13_23"

    def encryptionOne(prompt_text):
        password_secret1 = sign.encode_text(prompt_text,password)
        text1 = base64.b64encode(password_secret1.encode('utf-8'))
        result = text1.decode('utf-8')
        # prompt_output = st.text_area("输出",value=text1,height=200)
        return result

    def decryptionOne(prompt_text):
        result2 = sign.convertTostring(prompt_text)
        password_primary2 = sign.decode_text(result2,password)
        # result = password_primary2.decode('utf-8')
        # print(result)
        # prompt_output = st.text_area("输出",value=password_primary2,height=200)
        return password_primary2
    
    def encryptionMul(prompt_text):
        messages_str = json.dumps(prompt_text, ensure_ascii=False)
        messages = messages_str.replace("\\","")[1:-1]
        messages_base64_str = sign.string_to_base64(messages)
        true_sign = sign.encode_text(messages_base64_str,password)
        sign_base64 = sign.string_to_base64(true_sign)
        # prompt_output = st.text_area("输出",value=sign_base64,height=200)
        return sign_base64
    
    def decryptionMul(prompt_text):
        result2 = sign.convertTostring(prompt_text)
        password_primary2 = sign.decode_text(result2,password)
        result2 = sign.convertTostring(password_primary2)
        # prompt_output = st.text_area("输出",value=result2,height=200)
        return result2
    
    btn1.click(
        btn_encryption, 
        [input_letter,mode],
        [used_letters_box]
        )
    
    btn2.click(
        btn_decryption, 
        [input_letter,mode],
        [used_letters_box]
        )


# 如果要快捷分享，可以在下面添加参数：share=True
demo.launch(server_name='127.0.0.1',server_port=8080,favicon_path="./MiMouse.ico")